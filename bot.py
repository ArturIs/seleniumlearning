import telebot
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import exceptions
import sys
import time
import re



bot_token = None
with open('deploy/.some_secrets', 'r') as f:
    bot_token = re.search('(?<=bot_token=)[^\t\n" "]*', f.read()).group()
bot = telebot.TeleBot(bot_token)


capabilities = webdriver.FirefoxOptions().to_capabilities()
attempts, driver = 0, None
while attempts < 10:
    try:
        driver = webdriver.Remote(command_executor='http://selenium:4444/wd/hub', desired_capabilities=capabilities)
        break
    except:
        driver = None
        attempts += 1
        time.sleep(3)
if not driver:
    print("SELENIUM DOCKER CONTAINER UNAVAILABLE NOW")
    exit(-1)
print("BOT CONNECTED")



@bot.message_handler(commands=['start', 'help'])
def send_wellcome(message):
    bot.reply_to(message, "This bot search an image in google by description you provided")


NUM_OF_IMAGE_TO_SEARCH = 3
@bot.message_handler(content_types=['text'])
def echo_all(message):
    query = message.text
    sys.stderr.write(f"Query {query}\n")
    global driver
    try:
        driver.get('https://images.google.com/')
    except exceptions.WebDriverException as e:
        print(e.msg)
        driver = webdriver.Remote(command_executor='http://selenium:4444/wd/hub', desired_capabilities=capabilities)
        driver.get('https://images.google.com/')

    ##############################################################################################
    wait = WebDriverWait(driver, timeout=15, ignored_exceptions=[exceptions.NoSuchElementException])
    search_bar = driver.find_element(By.NAME, "q")
    search_bar.send_keys(query + Keys.ENTER)
    ##############################################################################################
    i, curr, count = 1, 1, NUM_OF_IMAGE_TO_SEARCH
    while curr <= count:
        xpath = f"/html/body/div[2]/c-wiz/div[3]/div[1]/div/div/div/div[1]/div[1]/span/div[1]/div[1]/div[{i}]/a[1]/div[1]/img"
        image = wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
        wait.until(EC.element_to_be_clickable, image)
        image.click()
        ##############################################################################################
        xpath = '/html/body/div[2]/c-wiz/div[3]/div[2]/div[3]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div[2]/div/a/img'
        image = wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
        url = image.get_attribute('src')
        print(f"url attempt 1: {url}")
        time_out = 0
        while not url.startswith('http'):
            time.sleep(1)
            url = image.get_attribute('src')
            time_out += 1
            print(f"url attempt {1+time_out}: {url}")
            if time_out == 2:
                break
        try:
            if url.startswith('http'):
                bot.send_photo(message.chat.id, url)
                curr += 1
        except:
            print(f"Error sending image #{i}")
        i += 1
        print('\n')


bot.polling()

source deploy/.env

echo $PYTHON_VERSION

python -m venv bot_venv && source bot_venv/bin/activate
pip install --upgrade pip && pip install -r bot_requirements.txt
python -u bot.py
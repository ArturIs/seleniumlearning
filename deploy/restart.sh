#!/bin/bash -l
if [ -z "$1" ]; then
    echo 'arg <project name>'
    exit 1
fi

apt update && apt install -y docker docker-compose

docker-compose down
docker-compose kill
docker volume prune --force

cp -f server_docker-compose.yml docker-compose.yml
#[ -f ./init.sh ] && ./init.sh || exit 1
docker-compose up --force-recreate --remove-orphans --detach
#[ -f ./post_init.sh ] && ./post_init.sh || exit 1
